﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClienteSoap;
using Entidades;

namespace ApiRest.Controllers
{
    public class RecibosController : ApiController
    {
        [HttpGet]
        [Route("api/recibos/consultarrecibo/{ClienteId}/{TipoRecibo}")]
        public object ConsultarRecibo(int ClienteId, string TipoRecibo)
        {
            var datosCliente = new ProxyRecibos().ConsultarRecibo(ClienteId, TipoRecibo);
            return datosCliente;
        }

        [HttpPost]
        [Route("api/factura/registrar")]
        public object RegistrarFactura(Factura Factura)
        {

            using (var context = new DB_EPM115Entities1())
            {
                try
                {

                    var result = context.Database
                    .ExecuteSqlCommand("Exc spRegistrarFactura @numContrato ,@ani, @ucid , @consultaExitosa, @tiempoLlamada"
                    , new SqlParameter("@numContrato", Factura.Contrato)
                    , new SqlParameter("@ani", Factura.Ani)
                    , new SqlParameter("@ucid", Factura.Ucid), new SqlParameter("@consultaExitosa", Factura.ConsultaExitosa)
                    , new SqlParameter("@tiempoLlamada", Factura.TiempoLlamada));
                    return result;
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        
    }
    }
}

        