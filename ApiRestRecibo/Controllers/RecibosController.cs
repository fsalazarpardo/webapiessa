﻿using ClienteSoap;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRestRecibo.Controllers
{
    public class RecibosController : ApiController
    {
            [HttpGet]
            [Route("api/recibos/consultarrecibo/{ClienteId}/{TipoRecibo}")]
            public object ConsultarRecibo(int ClienteId, string TipoRecibo)
            {
                var datosCliente = new ProxyRecibos().ConsultarRecibo(ClienteId, TipoRecibo);
                return datosCliente;
            }

            [HttpPost]
            [Route("api/factura/registrar")]
            public object RegistrarFactura(Llamada Llamada)
            {

                using (var context = new DB_EPM115Entities1())
                {
                    try
                    {

                        var result = context.Database
                        .ExecuteSqlCommand("spRegistrarFacturas @numContrato ,@ani, @ucid , @consultaExitosa, @tiempoLlamada"
                        , new SqlParameter("@numContrato", Llamada.Contrato)
                        , new SqlParameter("@ani", Llamada.Ani)
                        , new SqlParameter("@ucid", Llamada.Ucid), new SqlParameter("@consultaExitosa", Llamada.ConsultaExitosa)
                        , new SqlParameter("@tiempoLlamada", Llamada.TiempoLlamada));
                        return result;
                    }
                    catch (Exception ex)
                    {
                        return ex.Message;
                    }
                }

            }
        }
    }
