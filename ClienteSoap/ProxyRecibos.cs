﻿using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System;

namespace ClienteSoap
{
    public class ProxyRecibos
    {
        private void ValidarCertificadoSSL()
        {
            ServicePointManager.ServerCertificateValidationCallback = (snder, cert, chain, error) => true;
        }
        private bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


        public object ConsultarRecibo(int ClienteId, string TipoRecibo)
        {
            RespuestaServicio respuesta = new RespuestaServicio();
            try
            {
                ValidarCertificadoSSL();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                var cliente = new ServicioRecibos.RecibosClient();
                ServicioRecibos.reciboDAO[] datosFactura = cliente.consultaRecibo(ClienteId, TipoRecibo);

                var errorFactura = datosFactura.Where(f => f.orden == -1).FirstOrDefault();
                if (errorFactura != null)
                {
                    respuesta.respuesta_ws = datosFactura;
                    respuesta.codigo_resultado = "NOK";
                }
                else
                {
                    var datosFacturaFiltrados = datosFactura.Where(f => f.orden == 50 || f.orden == 51 || f.orden == 52 || f.orden == 53 || f.orden == 57).ToList();
                    respuesta.respuesta_ws = FiltrarDatosFactura(datosFacturaFiltrados);
                    respuesta.codigo_resultado = "OK";
                }
            }
            catch (Exception excepcion)
            {
                respuesta.respuesta_ws = excepcion.Message;
                respuesta.codigo_resultado = "NOK";
            }
            return respuesta;
        }

        private string ConvertirFecha(string fecha)
        {
            // System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("es-CO");
            //DateTime dt = DateTime.Parse(date, cultureinfo);
            string []  mesesEspanol = { "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO" , "SEP","OCT","NOV","DIC"};
            string[] mesesIngles = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

            Dictionary<string, string> meses = new Dictionary<string, string>();
            meses.Add("ENE", "JAN");
            meses.Add("JAN", "JAN");
            meses.Add("FEB", "FEB");
            meses.Add("MAR", "MAR");
            meses.Add("ABR", "APR");
            meses.Add("MAY", "MAY");
            meses.Add("JUN", "JUN");
            meses.Add("JUL", "JUL");
            meses.Add("AGO", "AUG");
            meses.Add("AUG", "AUG");
            meses.Add("SEP", "SEP");
            meses.Add("OCT", "OCT");
            meses.Add("NOV", "NOV");
            meses.Add("DIC", "DEC");
            meses.Add("DEC", "DEC");
            var split = fecha.Split('/');

            var mesSalida = string.Empty;
            String fechaSalida = string.Empty;

            if (meses.ContainsKey(split[0]))
            {
                mesSalida = meses[split[0]];
            }
            fechaSalida = mesSalida + "/" + split[1] + "/" + split[2];
            DateTime fechaConvertir;
            
            

            if (!DateTime.TryParse(fechaSalida, out fechaConvertir))
            {
                return fechaSalida;
            }
            else
            {
                fechaSalida = fechaConvertir.ToString("yyyy-MM-dd");
            }
            return fechaSalida;
        }

        private Factura FiltrarDatosFactura(List<ServicioRecibos.reciboDAO> datosfactura)
        {
            Factura Factura = new Factura();
            foreach (var item in datosfactura)
            {
                switch (item.orden)
                {
                    case 50:
                        Factura.ValorTotalPagar = item.dato.Trim();
                        break;
                    case 51:
                        Factura.FechaVencimiento = ConvertirFecha(item.dato.Trim());
                        break;
                    case 52:
                        Factura.FechaSuspension = ConvertirFecha(item.dato.Trim());
                        break;
                    case 53:
                        Factura.DiasFacturados = item.dato.Trim();
                        break;
                    case 57:
                        Factura.Atrasos = item.dato.Trim();
                        break;
                }

            }
            return Factura;
        }
    }
}
