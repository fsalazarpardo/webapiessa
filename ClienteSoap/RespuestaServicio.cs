﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteSoap
{
     public class RespuestaServicio
    {
        public object respuesta_ws
        {
            get;
            set;
        }

        public string codigo_resultado
        {
            get;
            set;
        }
    }
}
