﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Factura
    {
        public string ValorTotalPagar { get; set; }
        public string DiasFacturados { get; set; }
        public string FechaVencimiento { get; set; }
        public string FechaSuspension { get; set; }
        public string Atrasos { get; set; }

    }
}
