﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public  class Llamada
    {
        public string Ani { get; set; }
        public string Ucid { get; set; }
        public string Contrato { get; set; }
        public string ConsultaExitosa { get; set; }
        public string TiempoLlamada { get; set; }
    }
}
